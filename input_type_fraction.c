#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
this algorithm is to calculate the number of the positive integers and the negative integer and zero 
inputs and print the fraction of a given type of the input numbers
*/
int main(){
    int n; 
    scanf("%d",&n);
    int arr[n];
    int pos=0;
    int neg=0;
    int zer=0;
    for(int arr_i = 0; arr_i < n; arr_i++){
       scanf("%d",&arr[arr_i]);
        if (arr[arr_i]>0)
        {
            pos += 1;
        }
        else if (arr[arr_i]<0)
            {
            neg +=1;
        }else 
            {
            zer +=1;
        }
    }
    printf("%.6f",(float)pos/n);
    printf("\n%.6f",(float)neg/n);
    printf("\n%.6f",(float)zer/n);  
    printf("\n");  
    return 0;
}
